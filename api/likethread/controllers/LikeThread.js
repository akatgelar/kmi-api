'use strict';

/**
 * LikeThread.js controller
 *
 * @description: A set of functions called "actions" for managing `LikeThread`.
 */

module.exports = {

  /**
   * Retrieve likeThread records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.likeThread.fetchAll(ctx.query);
  },

  /**
   * Retrieve a likeThread record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.likeThread.fetch(ctx.params);
  },

  /**
   * Create a/an likeThread record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.likeThread.add(ctx.request.body);
  },

  /**
   * Update a/an likeThread record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.likeThread.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an likeThread record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.likeThread.remove(ctx.params);
  }
};
