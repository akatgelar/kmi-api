'use strict';

/**
 * FollowThread.js controller
 *
 * @description: A set of functions called "actions" for managing `FollowThread`.
 */

module.exports = {

  /**
   * Retrieve followThread records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.followThread.fetchAll(ctx.query);
  },

  /**
   * Retrieve a followThread record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.followThread.fetch(ctx.params);
  },

  /**
   * Create a/an followThread record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.followThread.add(ctx.request.body);
  },

  /**
   * Update a/an followThread record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.followThread.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an followThread record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.followThread.remove(ctx.params);
  }
};
