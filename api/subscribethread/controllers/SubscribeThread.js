'use strict';

/**
 * SubscribeThread.js controller
 *
 * @description: A set of functions called "actions" for managing `SubscribeThread`.
 */

module.exports = {

  /**
   * Retrieve subscribeThread records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.subscribeThread.fetchAll(ctx.query);
  },

  /**
   * Retrieve a subscribeThread record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.subscribeThread.fetch(ctx.params);
  },

  /**
   * Create a/an subscribeThread record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.subscribeThread.add(ctx.request.body);
  },

  /**
   * Update a/an subscribeThread record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.subscribeThread.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an subscribeThread record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.subscribeThread.remove(ctx.params);
  }
};
