'use strict';

/**
 * LikeComment.js controller
 *
 * @description: A set of functions called "actions" for managing `LikeComment`.
 */

module.exports = {

  /**
   * Retrieve likeComment records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.likeComment.fetchAll(ctx.query);
  },

  /**
   * Retrieve a likeComment record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.likeComment.fetch(ctx.params);
  },

  /**
   * Create a/an likeComment record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.likeComment.add(ctx.request.body);
  },

  /**
   * Update a/an likeComment record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.likeComment.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an likeComment record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.likeComment.remove(ctx.params);
  }
};
