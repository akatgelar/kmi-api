'use strict';

/**
 * FollowCategory.js controller
 *
 * @description: A set of functions called "actions" for managing `FollowCategory`.
 */

module.exports = {

  /**
   * Retrieve followCategory records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.followCategory.fetchAll(ctx.query);
  },

  /**
   * Retrieve a followCategory record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.followCategory.fetch(ctx.params);
  },

  /**
   * Create a/an followCategory record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.followCategory.add(ctx.request.body);
  },

  /**
   * Update a/an followCategory record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.followCategory.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an followCategory record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.followCategory.remove(ctx.params);
  }
};
