'use strict';

/**
 * Comment.js controller
 *
 * @description: A set of functions called "actions" for managing `Comment`.
 */

module.exports = {

  /**
   * Retrieve comment records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
	  
    //return strapi.services.comment.fetchAll(ctx.query);
	
	// Convert params.
    const formattedParams = strapi.utils.models.convertParams('comment', ctx.request.query);

    // Get the list of users according to the request query.
    const filteredUsers = await Comment
      .find()
      .where(formattedParams.where)
      .sort(formattedParams.sort)
      .skip(formattedParams.start)
      .limit(formattedParams.limit);

    // Finally, send the results to the client.
    ctx.body = filteredUsers;
	
	return ctx.body;
	
  },

  /**
   * Retrieve a comment record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.comment.fetch(ctx.params);
  },

  /**
   * Create a/an comment record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.comment.add(ctx.request.body);
  },

  /**
   * Update a/an comment record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.comment.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an comment record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.comment.remove(ctx.params);
  }
};
