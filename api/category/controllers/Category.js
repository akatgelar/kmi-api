'use strict';

/**
 * Category.js controller
 *
 * @description: A set of functions called "actions" for managing `Category`.
 */

module.exports = {

  /**
   * Retrieve category records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
	  
	/*
	// Convert params.
    const formattedParams = strapi.utils.models.convertParams('category', ctx.request.query);

    // Get the list of users according to the request query.
    const filteredUsers = await Category
      .find()
      .where(formattedParams.where)
      .sort(formattedParams.sort)
      .skip(formattedParams.start)
      .limit(formattedParams.limit);

    // Finally, send the results to the client.
    ctx.body = filteredUsers;
	
	return ctx.body;
	*/
	
    return strapi.services.category.fetchAll(ctx.query);
  },

  /**
   * Retrieve a category record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.category.fetch(ctx.params);
  },

  /**
   * Create a/an category record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.category.add(ctx.request.body);
  },

  /**
   * Update a/an category record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.category.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an category record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.category.remove(ctx.params);
  }
};
